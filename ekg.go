package ekg

import "sync/atomic"

// Gauge is mutable interger-valued gauges. Gauges could be used to track
// current number of open connections
type Gauge int64


// Set sets value x 
func (g *Gauge) Set(x int64) {
  atomic.StoreInt64((*int64)(g), x)
}

// Add will add value x to gauge value
func (g *Gauge) Add(x int64) {
  atomic.AddInt64((*int64)(g), x)
}

// Sub will subtrack value x from gauge
func (g *Gauge) Sub(x int64) {
  g.Add(^(x-1))
}


// Counter is mutable interger-valued counter.
// Counters are non-negative, monotonical incressing values
// and can be used to track requests numbers
type Counter uint64

// Inc incresses counter by one
func (c *Counter) Inc() {
  atomic.AddUint64((*uint64)(c), 1)
}

// Add adds value x to counter
func (c *Counter) Add(x uint64) {
  atomic.AddUint64((*uint64)(c), x)
}



