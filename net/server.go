package net

import (
  "gitlab.com/0nyx/go-ekg"
  net1 "net"
  "sync"
)

type metrics struct {
  OpenConns ekg.Gauge
}

type TrackListener struct {
  net1.Listener
  Metrics metrics
  ErrorCounter ekg.Counter
}

type TrackConn struct {
  net1.Conn
  once sync.Once
  release func()
}

func NewTrackListener(l net1.Listener) *TrackListener {
  return &TrackListener{Listener: l}
}


func (l *TrackListener) release()  {
  l.Metrics.OpenConns.Sub(1)
}

func (l *TrackListener) Accept() (net1.Conn, error)  {
  c, err := l.Listener.Accept()
  if err != nil {
    l.ErrorCounter.Inc()
    return nil, err
  }
  l.Metrics.OpenConns.Add(int64(1))

  return &TrackConn{Conn: c, release: l.release}, nil
}

func (l *TrackListener) Close() error {
  l.Metrics.OpenConns.Set(int64(0))
  return l.Listener.Close()
}


func (c *TrackConn) Close() error {
    c.once.Do(c.release)
    return c.Conn.Close()
}
