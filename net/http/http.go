package http

import (
  "gitlab.com/0nyx/go-ekg"
  http1 "net/http"
)

type metrics struct {
  TotalReq ekg.Counter
}

type TrackHandler struct {
  Handler http1.Handler
  Metrics metrics
}

func NewTrackHandler(h http1.Handler) *TrackHandler {
  return &TrackHandler{Handler: h}
}

func (h *TrackHandler) ServeHTTP(w http1.ResponseWriter, r *http1.Request) {
  h.Metrics.TotalReq.Inc()
  if h.Handler != nil {
    h.Handler.ServeHTTP(w, r)
  }
}
