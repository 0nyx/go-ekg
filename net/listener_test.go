package net

import (
  net1 "net"
  "testing"
)

func Test_TrackListener(t *testing.T) {
  for i:=0; i< 5;i++ {
    go func() {
      conn, err := net1.Dial("tcp", ":3000")
      if err != nil {
        t.Fatal(err)
      }
      defer conn.Close()
    }()
  }

  ln, err := net1.Listen("tcp", ":3000")
  if err != nil {
    t.Fatal(err)
  }
  t.Logf("before %d", 1)
  tLn := NewTrackListener(ln)
  defer tLn.Close()

  for {
    conn, err := tLn.Accept()
    if err != nil {
      t.Fatal(err)
    }
    defer conn.Close()
    if tLn.OpenConns == 5 {
      break
    }
  }
}
