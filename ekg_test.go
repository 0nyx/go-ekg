package ekg

import (
  "sync"
  "time"
  "math/rand"
  "testing"
)


func Test_CounterST(t *testing.T) {
  var c Counter

  // Lets do simple test first
  c.Inc()

  if uint(c) != 1 {
    t.Error("Test1: Counter.Inc did not increase the value")
  }

  c.Add(uint64(4))
  if uint(c) != 5 {
    t.Error("Test2: Counter.Add did not add the value")
  }

}


func Test_CounterMT(t *testing.T) {
  var wg sync.WaitGroup
  var c1 Counter

  for i:=0; i < 10; i++ {
    wg.Add(1)
    go func() {
      time.Sleep(time.Duration(rand.Intn(3)) *time.Second)
      c1.Inc()
      wg.Done()
    }()
  }
  wg.Wait()
  if uint(c1) != 10 {
    t.Error("Test3: Counter.Inc multi-thread inc faild: Expected: %d, Got: %d", 10, c1)
  }

  var c2 Counter
  for i:=0; i < 10; i++ {
    wg.Add(1)
    go func() {
      time.Sleep(time.Duration(rand.Intn(3)) *time.Second)
      c2.Add(10)
      wg.Done()
    }()
  }
  wg.Wait()
  if uint(c2) != 100 {
    t.Error("Test4: Counter.Add multi-thread inc faild: Expected: %d, Got: %d", 100, c2)
  }
}


func Test_GaugeST(t *testing.T) {
  var g Gauge

  // Lets do simple test first
  g.Add(1)

  if int(g) != 1 {
    t.Error("Test5: Gauge.Add did not increase the value")
  }

  g.Sub(4)
  if int(g) != -3 {
    t.Error("Test6: Gauge.Sub did not subtracted the value")
  }

  g.Set(1)
  if int(g) != 1 {
    t.Error("Test7: Gauge.Set did not set the value")
  }
}


func Test_GaugeMT(t *testing.T) {
  var wg sync.WaitGroup
  var g Gauge

  for i:=0; i < 10; i++ {
    wg.Add(1)
    go func() {
      time.Sleep(time.Duration(rand.Intn(3)) *time.Second)
      g.Add(1)
      wg.Done()
    }()
  }
  wg.Wait()
  if uint(g) != 10 {
    t.Error("Test3: Counter.Inc multi-thread inc faild: Expected: %d, Got: %d", 10, g)
  }

  for i:=0; i < 10; i++ {
    wg.Add(1)
    go func() {
      time.Sleep(time.Duration(rand.Intn(3)) *time.Second)
      g.Sub(3)
      wg.Done()
    }()
  }
  wg.Wait()
  if g != -20 {
    t.Error("Test4: Counter.Add multi-thread inc faild: Expected: %d, Got: %d", -20, g)
  }
}
